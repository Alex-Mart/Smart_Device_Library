package org.zwave4j;

//import MAS.devices.Device;

import java.util.ArrayList;

/**
 * Created by alex on 3/23/16.
 */
public class Node {
    private short nodeID;
    //private ValueId valueId
    private ArrayList<ValueId> valueIdList;
    private ArrayList<Short> commandClasses;
    private String manufacturerName;
    private String productName;
    private String protocolInfo;
    private double currentConsumption;
    private double totalConsumption;
    private int changes;
    private long startTime;
    private int timeSlot;
    private double [] timeStamp;
    private double temperature;


    //private Device device;

    public Node(short ID, String manufacturer, String product){//}, ArrayList<ValueId> vID){
        nodeID = ID;
        manufacturerName = manufacturer;
        productName = product;
        currentConsumption = 0;
        totalConsumption = 0;
        changes = 1;
        startTime = System.currentTimeMillis();
        timeSlot = 1;
        timeStamp = new double[60];
        valueIdList = new ArrayList<>();
        commandClasses = new ArrayList<Short>();
    }



    public void addValueID(ValueId vID){
        valueIdList.add(vID);
        if(!commandClasses.contains(vID.getCommandClassId())){
            commandClasses.add(vID.getCommandClassId());
        }
    }

    public ArrayList<ValueId> getValueList(){
        return valueIdList;
    }

    public void addCurrentConsumption(double consumption){
        currentConsumption = consumption;
    }

    public void addTotalConsumption(double consumption){

        if(consumption == 0.0)
            return;
        else
            totalConsumption = consumption;
        /*
        changes++;
        currentConsumption = (currentConsumption + consumption)/changes;
       //device.setPowerConsumption((float)currentConsumption);

        if(((System.currentTimeMillis()-startTime)/1000)>60*timeSlot){
            timeStamp[timeSlot-1] = currentConsumption;
            timeSlot++;
        }
        */

    }

    public void addTimeConsumption(){

    }

    public double getTimeStamp(int time){
        return timeStamp[time];
    }


    public double getCurrentConsumption(){
        return currentConsumption/1000;
    }

    public double getTotalConsumption(){
        return totalConsumption;
    }

    public short getID(){
        return nodeID;
    }

    public String getManufacturerName(){
        return manufacturerName;
    }

    public String getProductName(){
        return productName;
    }

    public String getProtocolInfo(){
        return protocolInfo;
    }

    public ValueId getValueID(short commandClass, short index){
        return valueIdList.get(valueIdList.indexOf(new ValueId(commandClass,index)));
    }

    public boolean valueIDExists(short commandClass, short index){
        return valueIdList.contains(valueIdList.indexOf(new ValueId(commandClass, index)));
    }

    public double getTemparature(){
        return temperature;
    }


    //Mutators

    public void setID(short ID){
        nodeID = ID;
        //device.setActualID(ID);
    }

    public void setManufacturerName(String mName){
        manufacturerName = mName;
    }

    public void setProductName(String pName){
        productName = pName;
    }

    public void setProtocolInfo(String pInfo){
        protocolInfo = pInfo;
    }

    public void setTemperature(double temp){
        temperature = temp;
    }


    public boolean equals(Object o){
        if(o instanceof Node){
            Node toCompare = (Node) o;
            return this.nodeID == toCompare.getID();
        }
        return false;
    }

    public String toString() {
        return "Manufacturer: " + manufacturerName +
                "\n Product: " + productName +
                "\n Device Type: " + protocolInfo +
                "\n";
    }



}
