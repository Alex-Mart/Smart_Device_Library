package org.zwave4j;/*
 * Copyright (c) 2013 Alexander Zagumennikov
 *
 * SOFTWARE NOTICE AND LICENSE
 *
 * This file is part of ZWave4J.
 *
 * ZWave4J is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ZWave4J is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ZWave4J.  If not, see <http://www.gnu.org/licenses/>.
 */


import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

/**
 * @author zagumennikov
 * @author Alejandro Martinez
 */



public class SDManager implements  Runnable {

    private static long homeId;
    private static boolean ready;
    private static Manager manager;
    final private static String controllerPort = "/dev/ttyACM0";
    private static NotificationWatcher watcher;
    private static boolean commandStatus;


    private static List<Node> nodelist = new ArrayList<Node>();

    //Run method for separate thread
    public void run(){
        try{
            start();
        }catch(Throwable e){
            System.out.println("Cannot run");
        }
    }

    /**
      start method initiates the manager, the notification watcher, and sets the port for the ZWave stick.
      This method is constantly running as it holds the notification watcher that watches for any changes
      the devices (ex. power intake, on/off status)
     */

    public void start() throws IOException {

        try {
            /* Pre setup to allow SDManager to find C++ binaries, config folder for Manufacturer specific parameters
            * As well as extract the config directory to the current directory */
                ready = false;
                commandStatus = false;

                String libraryPath = SDManager.class.getProtectionDomain().getCodeSource().getLocation().getPath();
                Runtime.getRuntime().exec("jar xf " + libraryPath + " " + "./ config").waitFor();
                NativeLibraryLoader.loadLibrary(ZWave4j.LIBRARY_NAME, ZWave4j.class);

                final Options options = Options.create("./config","", "");
                options.addOptionBool("ConsoleOutput", false);
                options.lock();

                manager = Manager.create();

                /*Notification watcher that allows the program to view interactions between the Z-Stick and each
                connected device. Has various cases */

                watcher = new NotificationWatcher() {
                    @Override
                    public void onNotification(Notification notification, Object context) {



                        switch (notification.getType()) {
                            case DRIVER_READY:
                                System.out.println(String.format("Driver ready\n" +
                                                "\thome id: %d",
                                        notification.getHomeId()
                                ));
                                homeId = notification.getHomeId();
                                break;
                            case DRIVER_FAILED:
                                System.out.println("Driver failed");
                                break;
                            case DRIVER_RESET:
                                System.out.println("Driver reset");
                                break;
                            case AWAKE_NODES_QUERIED:
                                System.out.println("Awake nodes queried");
                                //manager.writeConfig(homeId);
                                //ready = true;
                                break;
                            case ALL_NODES_QUERIED:
                                System.out.println("All nodes queried");
                                manager.writeConfig(homeId);
                                ready = true;
                                //System.out.println(ready);
                                break;
                            case ALL_NODES_QUERIED_SOME_DEAD:
                                System.out.println("All nodes queried some dead");
                                manager.writeConfig(homeId);
                                ready = true;
                                //System.out.println(ready);
                                break;
                            case POLLING_ENABLED:
                                System.out.println("Polling enabled");
                                break;
                            case POLLING_DISABLED:
                                System.out.println("Polling disabled");
                                break;
                            case NODE_NEW:
                                System.out.println(String.format("Node new\n" +
                                                "\tnode id: %d",
                                        notification.getNodeId()
                                ));
                                break;
                            case NODE_ADDED:
                                System.out.println(String.format("Node added\n" +
                                                "\tnode id: %d" +
                                                "\tmanufacturer: %s" +
                                                "\tproduct: %s",
                                        notification.getNodeId(),
                                        manager.getNodeManufacturerName(homeId,notification.getNodeId()),
                                        manager.getNodeProductName(homeId,notification.getNodeId())
                                ));
                                if(commandStatus)
                                    manager.cancelControllerCommand(homeId);

                                Node temp = new Node(notification.getNodeId(),
                                        manager.getNodeManufacturerName(homeId, notification.getNodeId()),
                                        manager.getNodeProductName(homeId,notification.getNodeId()));
                                temp.addValueID(notification.getValueId());
                                nodelist.add(temp);
                                break;
                            case NODE_REMOVED:
                                System.out.println(String.format("Node removed\n" +
                                                "\tnode id: %d",
                                        notification.getNodeId()
                                ));
                                if(nodelist.contains(new Node(notification.getNodeId(),"",""))){
                                    nodelist.remove(nodelist.indexOf(new Node(notification.getNodeId(),"","")));
                                }

                                break;
                            case ESSENTIAL_NODE_QUERIES_COMPLETE:
                                System.out.println(String.format("Node essential queries complete\n" +
                                                "\tnode id: %d",
                                        notification.getNodeId()
                                ));
                                break;
                            case NODE_QUERIES_COMPLETE:
                                System.out.println(String.format("Node queries complete\n" +
                                                "\tnode id: %d",
                                        notification.getNodeId()
                                ));
                                break;
                            case NODE_EVENT:
                                System.out.println(String.format("Node event\n" +
                                                "\tnode id: %d\n" +
                                                "\tevent id: %d",
                                        notification.getNodeId(),
                                        notification.getEvent()
                                ));
                                break;
                            case NODE_NAMING:
                                System.out.println(String.format("Node naming\n" +
                                                "\tnode id: %d",
                                        notification.getNodeId()
                                ));
                                if(nodelist.get(nodelist.indexOf(new Node(notification.getNodeId(),"",""))).getProductName().equals("")
                                        || nodelist.get(nodelist.indexOf(new Node(notification.getNodeId(),"",""))).getManufacturerName().equals("")){
                                    nodelist.get(nodelist.indexOf(new Node(notification.getNodeId(),"",""))).setProductName(manager.getNodeProductName(homeId,notification.getNodeId()));
                                    nodelist.get(nodelist.indexOf(new Node(notification.getNodeId(),"",""))).setManufacturerName(manager.getNodeManufacturerName(homeId, notification.getNodeId()));

                                }

                                break;
                            case NODE_PROTOCOL_INFO:
                                System.out.println(String.format("Node protocol info\n" +
                                                "\tnode id: %d\n" +
                                                "\ttype: %s",
                                        notification.getNodeId(),
                                        manager.getNodeType(notification.getHomeId(), notification.getNodeId())
                                ));
                                nodelist.get(nodelist.indexOf(
                                        new Node(notification.getNodeId(),"",""))).setProtocolInfo(
                                        manager.getNodeType(homeId,notification.getNodeId()));
                                break;
                            case VALUE_ADDED:
                                System.out.println(String.format("Value added\n" +
                                                "\tnode id: %d\n" +
                                                "\tcommand class: %d\n" +
                                                "\tinstance: %d\n" +
                                                "\tindex: %d\n" +
                                                "\tgenre: %s\n" +
                                                "\ttype: %s\n" +
                                                "\tlabel: %s\n" +
                                                "\tvalue: %s\n" +
                                                "\tunits: %s",
                                        notification.getNodeId(),
                                       notification.getValueId().getCommandClassId(),
                                       notification.getValueId().getInstance(),
                                       notification.getValueId().getIndex(),
                                       notification.getValueId().getGenre().name(),
                                       notification.getValueId().getType().name(),
                                       manager.getValueLabel(notification.getValueId()),
                                       getValue(notification.getValueId()),
                                       manager.getValueUnits(notification.getValueId())
                                ));

                                if(nodelist.get(nodelist.indexOf(new Node(notification.getNodeId(),"",""))).getProductName().equals("")
                                    || nodelist.get(nodelist.indexOf(new Node(notification.getNodeId(),"",""))).getManufacturerName().equals("")){
                                    nodelist.get(nodelist.indexOf(new Node(notification.getNodeId(),"",""))).setProductName(manager.getNodeProductName(homeId,notification.getNodeId()));
                                    nodelist.get(nodelist.indexOf(new Node(notification.getNodeId(),"",""))).setManufacturerName(manager.getNodeManufacturerName(homeId, notification.getNodeId()));

                                }

                                ValueId vID = notification.getValueId();
                                vID.setLabel(manager.getValueLabel(vID));
                                vID.setUnits(manager.getValueUnits(vID));

                                Node check = new Node(notification.getNodeId(),
                                        manager.getNodeManufacturerName(homeId,notification.getNodeId()),
                                        manager.getNodeProductId(homeId,notification.getNodeId()));
                                nodelist.get(nodelist.indexOf(check)).addValueID(vID);

                                break;
                            case VALUE_REMOVED:
                                System.out.println(String.format("Value removed\n" +
                                                "\tnode id: %d\n" +
                                                "\tcommand class: %d\n" +
                                                "\tinstance: %d\n" +
                                                "\tindex: %d",
                                        notification.getNodeId(),
                                        notification.getValueId().getCommandClassId(),
                                        notification.getValueId().getInstance(),
                                        notification.getValueId().getIndex()
                                ));
                                break;
                            case VALUE_CHANGED:

                                System.out.println(String.format("Value changed\n" +
                                                "\tnode id: %d\n" +
                                                "\tcommand class: %d\n" +
                                                "\tinstance: %d\n" +
                                                "\tindex: %d\n" +
                                               "\tvalue: %s" +
                                                "\ttype: %s",
                                        notification.getNodeId(),
                                        notification.getValueId().getCommandClassId(),
                                        notification.getValueId().getInstance(),
                                        notification.getValueId().getIndex(),
                                       getValue(notification.getValueId()),
                                               notification.getValueId().getType()


                                ));
                                System.out.println();
                                System.out.println(manager.getValueLabel(notification.getValueId()));
                                System.out.println(manager.getValueUnits(notification.getValueId()));

                                if(manager.getValueLabel(notification.getValueId()).equals("Power")
                                        && notification.getValueId().getIndex() == 8){
                                    nodelist.get(nodelist.indexOf(
                                            new Node(notification.getNodeId(),"",""))).addCurrentConsumption(
                                            Double.parseDouble(getValue(notification.getValueId()).toString()));
                                }

                                if(notification.getValueId().getCommandClassId() == 49 &&
                                        notification.getValueId().getIndex() == 1 && manager.getValueUnits(notification.getValueId()).equals("F")){
                                    nodelist.get(nodelist.indexOf(
                                            new Node(notification.getNodeId(),"",""))).setTemperature(
                                            Double.parseDouble(getValue(notification.getValueId()).toString()));
                                }



                                break;
                            case VALUE_REFRESHED:

                                System.out.println(String.format("Value refreshed\n" +
                                                "\tnode id: %d\n" +
                                                "\tcommand class: %d\n" +
                                                "\tinstance: %d\n" +
                                                "\tindex: %d" +
                                                "\tvalue: %s",
                                        notification.getNodeId(),
                                        notification.getValueId().getCommandClassId(),
                                        notification.getValueId().getInstance(),
                                        notification.getValueId().getIndex(),
                                        getValue(notification.getValueId())
                                ));
                                if(manager.getValueLabel(notification.getValueId()).equals("Power")
                                        && notification.getValueId().getIndex() == 8){
                                    nodelist.get(nodelist.indexOf(
                                            new Node(notification.getNodeId(),"",""))).addCurrentConsumption(
                                            Double.parseDouble(getValue(notification.getValueId()).toString()));
                                }
                                System.out.println("refreshed");
                                    break;
                            case GROUP:
                                System.out.println(String.format("Group\n" +
                                                "\tnode id: %d\n" +
                                                "\tgroup id: %d",
                                        notification.getNodeId(),
                                        notification.getGroupIdx()
                                ));
                                //groupID.add(notification.getGroupIdx());
                                break;

                            case SCENE_EVENT:
                                System.out.println(String.format("Scene event\n" +
                                                "\tscene id: %d",
                                        notification.getSceneId()
                                ));
                                break;
                            case CREATE_BUTTON:
                                System.out.println(String.format("Button create\n" +
                                                "\tbutton id: %d",
                                        notification.getButtonId()
                                ));
                                break;
                            case DELETE_BUTTON:
                                System.out.println(String.format("Button delete\n" +
                                                             "\tbutton id: %d",
                                      notification.getButtonId()
                                ));
                                break;
                            case BUTTON_ON:
                                System.out.println(String.format("Button on\n" +
                                              "\tbutton id: %d",
                                     notification.getButtonId()

                                ));
                                break;
                            case BUTTON_OFF:
                                System.out.println(String.format("Button off\n" +
                                              "\tbutton id: %d",
                                    notification.getButtonId()
                                ));
                                break;
                            case NOTIFICATION:
                                //System.out.println("Notification");
                                break;
                            default:
                                System.out.println(notification.getType().name());
                                break;
                        }
                    }
                };

                /*Adds the notification watcher and the serial controller port to the manager object*/

                manager.addWatcher(watcher, null);
                manager.addDriver(controllerPort);

                /*While statement that loops until the system is ready. There is a chance that queries to devices that are on the network
                but are not fully initialized can cause the system to crash */

                while(!ready)
                    Thread.sleep(1000);

                nodelist.forEach(System.out::println);

                /*Will continue running as long as the ready conditions is true, once false will begin shutdown.
                Synchronized method calls allow this to run in parallel with any program calling the library */

                while(ready)
                    Thread.sleep(100);


            /*Shuts down the Smart Device Manager and destroys all variables*/

            System.out.println("Shutting Down Manager");
            manager.removeWatcher(watcher, null);
            manager.removeDriver(controllerPort);
            Manager.destroy();
            Options.destroy();

        }catch(Throwable e){
            System.out.println(e);
            System.out.println("NO GOOD");
        }
    }

    /**
     * Returns the node list to the callee (Should be taken out in the future as it may be unsafe. Mostly used for testing)
     * @return List
     */
     public static synchronized List<Node> getNodeList(){
         return nodelist;
     }


    /**
     * Scans the all the nodes in the node list and returns the aggregated power consumption
     * @return The total power consumption of all nodes
     */
    public static synchronized double getTotalConsumption(){
        double temp = 0;
        for(int i = 0; i< nodelist.size();i++)
            temp += nodelist.get(i).getCurrentConsumption();
        return temp;
    }

    /**
     * Uses the built in method to query the node for power consumption by passing in the command class and the index
     * If the node does not support the power consumption class this system will crash (Must fix)
     * @param nodeID ID of the desired device
     * @return returns the powerConsumption as a double 
     */
    public static synchronized double getPowerConsumption(short nodeID){
        AtomicReference<Float> temp = new AtomicReference<Float>();
        manager.getValueAsFloat(nodelist.get(nodelist.indexOf(new Node(nodeID,"",""))).getValueID(
                (short)50,(short)8),temp);
        return temp.get().doubleValue();
    }

    /**
     * Returns the manufacturer name of the desired node stored in the Node data structure (It is saved upon initialization)
     * @param nodeID
     * @return returns the manufacturer name as a String
     */
    public static synchronized String getManufacturerName (short nodeID){
        return manager.getNodeManufacturerName(homeId, nodeID);
    }

    /**
     * Returns the product name of the desired node stored in the Node data structure (It is saved upon initialization)
     * @param nodeID
     * @return returns the product name as a String
     */
    public static synchronized String getProductName(short nodeID){
        return manager.getNodeProductName(homeId, nodeID);
    }

    /**
     * Returns the type of device the node is stored in the Node data structure (Saved upon initialization)
     * @param nodeID
     * @return
     */
    public static synchronized String getProtocolInfo(short nodeID){
        return nodelist.get(nodelist.indexOf(new Node(nodeID,"",""))).getProtocolInfo();
    }

    /**
     * Button Method (unused and irrelevant at the moment)
     */
    public static synchronized void button(){
        manager.pressButton(nodelist.get(0).getValueList().get(0));
    }

    /**
     * Returns the product type of the desired node saved in the Node data structure (Saved upon initialization)
     * @param nodeID
     * @return
     */
    public static synchronized String getProductType(short nodeID){
        return manager.getNodeType(homeId, nodeID);
    }

    /**
     * Returns the devices status of the desired node saved int the Node data structure (Is non functional at the moment)
     * @param nodeID
     * @return
     */
    public static synchronized boolean deviceStatus(short nodeID) {
        return manager.isNodeAwake(homeId, nodeID);
    }

    /**
     * Switches the ready variable to false to allow the system to shutdown
     */
    public synchronized void shutDown(){
        ready = false;
    }

    /**
     * Returns the status of the system (If is ready or not)
     * @return boolean value of ready
     */
    public static synchronized boolean isReady(){
        return ready;
    }

    /**
     * Switches the desired node on if it supports this feature through the command class
     * Note: Will cause the system to crash if it doesn not support this (Must fix)
     * @param nodeID
     * @return boolean value of the status of the switch
     */
    public static synchronized boolean setOn(short nodeID) {
        return manager.setValueAsBool(nodelist.get(nodelist.indexOf(new Node(nodeID, "", ""))).getValueID((short) 0x25, (short) 0), true);
    }

    /**
     * Switches the desired node off if it supports this feature through the command class
     * Note: Will cause the system to crash if it doesn not support this (Must fix)
     * @param nodeID
     * @return boolean value of the status of the switch
     */
    public static synchronized boolean setOff(short nodeID){
        //if(nodelist.get(nodelist.indexOf(new Node(ID,"",""))).getValueID(short)0x25,(short)0)
        return manager.setValueAsBool(nodelist.get(nodelist.indexOf(new Node(nodeID, "", ""))).getValueID((short) 0x25, (short) 0), false);
    }

    /**
     * Switches the desired node on using built in method (only works with some nodes)
     * @param nodeID
     */
    public static synchronized void setOn2(short nodeID){

        manager.setNodeOn(homeId, nodeID);
    }

    /**
     * Switches the desired node off using build in method (only works with some nodes)
     * @param nodeID
     */
    public static synchronized void setOff2(short nodeID){
        manager.setNodeOff(homeId, nodeID);
    }

    /**
     * Sets the node at a desired level (0-100 short value) only if the node supports the command class
     * Note: Will cause system to crash if node does not support it
     * @param nodeID
     * @param level
     */
    public static synchronized void setLevel(short nodeID, int level) {
        //if(nodelist.get(nodelist.indexOf(new Node(nodeID,"",""))).valueIDExists((short)0x50,(short)0))
            manager.setValueAsByte(nodelist.get(nodelist.indexOf(new Node(nodeID, "", ""))).getValueID((short) 38, (short) 0), (short) level);
        //System.out.println("Device doesn't support level");
    }

    /**
     * Testing method to activate integrated button on nodes (Does not work)
     * @param nodeID
     * @param commandClass
     * @param index
     */
    public static synchronized void pushButton(short nodeID, short commandClass, short index){
        manager.pressButton(nodelist.get(nodelist.indexOf(new Node(nodeID,"",""))).getValueID(commandClass,index));
    }

    /**
     * Prints the command classes stored for a desired node on to the terminal
     * @param nodeID
     */
    public static synchronized void printCommandClasses(short nodeID){
        Node temp = nodelist.get(nodelist.indexOf(new Node(nodeID,"","")));

        System.out.println("Manufacturer: " + temp.getManufacturerName());
        System.out.println("Product: " + temp.getProductName());
        for(int j = 0; j<temp.getValueList().size(); j++){
            System.out.println("Command Class: " + temp.getValueList().get(j).getCommandClassId());
            System.out.println(temp.getValueList().get(j));
        }
        System.out.println();
    }

    /**
     * Built in method to turn all nodes on
     */
    public static synchronized void setAllOn(){
        manager.switchAllOn(homeId);
    }

    /**
     * Built in method to turn all nodes off
     */
    public static synchronized void setAllOff(){
        manager.switchAllOff(homeId);
    }

    /**
     * Returns the device name of a desired node stored in the Node data structure (Saved upon initializing)
     * @return
     */
    public static synchronized String getDeviceName(){
        return manager.getNodeName(homeId, nodelist.get(2).getID());
    }

    /**
     * Method to make controller go into pairing mode and allow user to add a node to it
     */
    public static synchronized void addNode(){
        commandStatus = manager.beginControllerCommand(homeId,ControllerCommand.ADD_DEVICE);
    }

    /**
     * Method to make controller go into unpairing mode and allow user to remove node from it
     */
    public static synchronized void removeNode(){
        manager.beginControllerCommand(homeId,ControllerCommand.REMOVE_DEVICE);
    }

    /**
     * Stops the pairing/unpairing commands
     */
    public static synchronized void stopCommand(){
        manager.cancelControllerCommand(homeId);
    }

    /**
     * Hard resets the controller, deleting all nodes from memory
     * WARNING: Do not do this in the middle of controlling a node as it may cause unwanted behaviors
     */
    public static synchronized void hardReset(){
        manager.resetController(homeId);
    }

    /**
     * Resets the controller (turn off and on)
     */
    public static synchronized void softReset(){ manager.softReset(homeId);}

    /**
     * Method to control the cooling portion of a thermostat
     * @param nodeID
     * @param temp
     */
    public static synchronized void coolTemp(short nodeID, float temp){
        manager.setValueAsFloat(nodelist.get(nodelist.indexOf(new Node(nodeID, "", ""))).getValueID((short) 67, (short) 2), temp);
    }

    /**
     * Method to control the heating portion of a thermostat
     * @param nodeID
     * @param temp
     */
    public static synchronized void heatTemp(short nodeID, float temp){
        manager.setValueAsFloat(nodelist.get(nodelist.indexOf(new Node(nodeID,"",""))).getValueID((short) 67, (short) 1),temp);
    }

    /**
     * Returns the temperature recored from the temperature sensor of a node if it contains it, using the command class
     * Note: Will cause the system to crash if the node does not support this command class
     * @param nodeID
     * @return
     */
    public static synchronized double getTemperature(short nodeID){
        //return nodelist.get(nodelist.indexOf(new Node(ID,"",""))).getTemparature();
        AtomicReference<Float> temp = new AtomicReference<Float>();
        manager.getValueAsFloat(nodelist.get(nodelist.indexOf(
                new Node(nodeID, "", ""))).getValueID((short) 49, (short) 1), temp);
        return temp.get().doubleValue();

    }

    /**
     * Method to change the operating state of a node using the command class (Varies depending on devices)
     * Note: Will cause system to crash if node does not support this command class
     * @param nodeID
     * @param state
     */
    public static synchronized void changeOpertingState(short nodeID, String state){
        manager.setValueListSelection(nodelist.get(nodelist.indexOf(new Node((short) nodeID, "", ""))).getValueID((short) 64, (short) 0), state);
    }

    /**
     * Method to change how frequently a node wakes up and transmits data to the controller
     * @param nodeID
     * @param interval
     */
    public static synchronized void changeWakeUpInterval(short nodeID, int interval){
        manager.setValueAsInt(nodelist.get(nodelist.indexOf(new Node(nodeID, "", ""))).getValueID((short) 132, (short) 1), interval);
    }

    /**
     * Method to change the polling interval the controller uses to query all nodes
     * Note: Can cause system to hang if there are too many queries
     * @param interval
     */
    public static synchronized void setPollingInterval(int interval){
        manager.setPollInterval(interval, true);
    }

    /**
     * Method to enable periodic polling to the nodes
     * @param nodeID
     * @param command
     * @param index
     */
    public static synchronized void setPolling(short nodeID, short command, short index){
        manager.enablePoll(nodelist.get(nodelist.indexOf(new Node(nodeID, "", ""))).getValueID(command, index));
    }

    /**
     * Method to get the value of the desired command class as an integer if it supports it
     * @param nodeID
     * @param command
     * @param index
     * @return
     */
    public static synchronized int getValueInt(short nodeID, short command, short index){
        AtomicReference<Integer> temp = new AtomicReference<>();
        manager.getValueAsInt(nodelist.get(nodelist.indexOf(new Node(nodeID, "", ""))).getValueID(command, index),
                temp);
        return temp.get();
    }

    /**
     * Method to set the desired command class to a certain integer if the command class supports it
     * @param nodeID
     * @param command
     * @param index
     * @param value
     */
    public static synchronized void setValueInt(short nodeID,short command, short index, int value){
        manager.setValueAsInt(nodelist.get(nodelist.indexOf(new Node(nodeID,"",""))).getValueID(command,index),value);
    }

    private static Object getValue(ValueId valueId) {
        switch (valueId.getType()) {
            case BOOL:
                AtomicReference<Boolean> b = new AtomicReference<>();
                Manager.get().getValueAsBool(valueId, b);
                return b.get();
            case BYTE:
                AtomicReference<Short> bb = new AtomicReference<>();
                Manager.get().getValueAsByte(valueId, bb);
                return bb.get();
            case DECIMAL:
                AtomicReference<Float> f = new AtomicReference<>();
                Manager.get().getValueAsFloat(valueId, f);
                return f.get();
            case INT:
                AtomicReference<Integer> i = new AtomicReference<>();
                Manager.get().getValueAsInt(valueId, i);
                return i.get();
            case LIST:
                return null;
            case SCHEDULE:
                return null;
            case SHORT:
                AtomicReference<Short> s = new AtomicReference<>();
                Manager.get().getValueAsShort(valueId, s);
                return s.get();
            case STRING:
                AtomicReference<String> ss = new AtomicReference<>();
                Manager.get().getValueAsString(valueId, ss);
                return ss.get();
            case BUTTON:
                return null;
            case RAW:
                AtomicReference<short[]> sss = new AtomicReference<>();
                Manager.get().getValueAsRaw(valueId, sss);
                return sss.get();
            default:
                return null;

        }

    }
}
